<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Skills
 */
class Skills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('skills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('key', 60)->unique();
            $table->smallInteger('order')->nullable();
            $table->integer('percent')->nullable();
            $table->enum('category', ['front', 'back', "dataBase", "other"]);
            $table->string('warning_text')->nullable();
            $table->timestamp("created_at")->useCurrent();
            $table->timestamp("updated_at")->nullable();
            $table->softDeletes();
        });

        \Illuminate\Support\Facades\DB::table("skills")->insert([
            [ "name" => "PHP", "key" => "php", "order" => 1, "percent" => 100, "category" => "back", "warning_text" => null ],
            [ "name" => "LaravelPHP", "key" => "laravelPhp", "order" => 1, "percent" => 100, "category" => "back", "warning_text" => "Uso o framework desde a versão 4.2 e tenho acompanhado novas versões" ],
            [ "name" => "NodeJs", "key" => "nodeJs", "order" => 1, "percent" => 90, "category" => "back", "warning_text" => null ],
            [ "name" => "Express", "key" => "express", "order" => 2, "percent" => 90, "category" => "back", "warning_text" => null ],
            [ "name" => "ORM Eloquent", "key" => "eloquent", "order" => 3, "percent" => 100, "category" => "back", "warning_text" => null ],
            [ "name" => "ORM Sequelize", "key" => "sequelize", "order" => 3, "percent" => 70, "category" => "back", "warning_text" => null ],

            [ "name" => "MySql", "key" => "mySql", "order" => 1, "percent" => 100, "category" => "dataBase", "warning_text" => null ],
            [ "name" => "MongoDB", "key" => "mongoDb", "order" => 2, "percent" => 70, "category" => "dataBase", "warning_text" => null ],
            [ "name" => "Redis", "key" => "redis", "order" => 1, "percent" => 80, "category" => "dataBase", "warning_text" => null ],
            [ "name" => "ElasticSearch", "key" => "elasticSearch", "order" => 3, "percent" => 60, "category" => "dataBase", "warning_text" => null ],
            [ "name" => "SqLite", "key" => "sqLite", "order" => 3, "percent" => 80, "category" => "dataBase", "warning_text" => null ],

            [ "name" => "React", "key" => "react", "order" => 2, "percent" => 70, "category" => "front", "warning_text" => null ],
            [ "name" => "React Native", "key" => "reactNative", "order" => 2, "percent" => 60, "category" => "front", "warning_text" => null ],
            [ "name" => "HTML", "key" => "html", "order" => 1, "percent" => 100, "category" => "front", "warning_text" => null ],
            [ "name" => "Bootstrap 3 - 4", "key" => "bootstrap", "order" => 1, "percent" => 100, "category" => "front", "warning_text" => null ],
            [ "name" => "JavaScript", "key" => "javaScript", "order" => 1, "percent" => 85, "category" => "front", "warning_text" => null ],
            [ "name" => "CSS", "key" => "CSS", "order" => 1, "percent" => 95, "category" => "front", "warning_text" => "Não tenho muito senso estético apenas" ],
            [ "name" => "SCSS", "key" => "SCSS", "order" => 2, "percent" => 95, "category" => "front", "warning_text" => "Não tenho muito senso estético apenas" ],
            [ "name" => "JQuery", "key" => "jQuery", "order" => 3, "percent" => 100, "category" => "front", "warning_text" => null ],
            [ "name" => "Redux", "key" => "redux", "order" => 2, "percent" => 100, "category" => "front", "warning_text" => null ],
            [ "name" => "Java Android", "key" => "java", "order" => 3, "percent" => 40, "category" => "front", "warning_text" => null ],
            [ "name" => "Kotlin", "key" => "kotlin", "order" => 3, "percent" => 40, "category" => "front", "warning_text" => null ],

            [ "name" => "Git", "key" => "git", "order" => 1, "percent" => 100, "category" => "other", "warning_text" => null ],
            [ "name" => "GitFlow", "key" => "gitFlow", "order" => 1, "percent" => 100, "category" => "other", "warning_text" => null ],
            [ "name" => "Api Rest", "key" => "apiRest", "order" => 1, "percent" => 100, "category" => "other", "warning_text" => null ],
            [ "name" => "Api Rest Full", "key" => "apiRestFull", "order" => 1, "percent" => 100, "category" => "other", "warning_text" => null ],
            [ "name" => "Auth / OAuth", "key" => "auth", "order" => 2, "percent" => 100, "category" => "other", "warning_text" => null ],
            [ "name" => "WebPack", "key" => "webPack", "order" => 2, "percent" => 60, "category" => "other", "warning_text" => null ],
            [ "name" => "Design Pattern", "key" => "designPattern", "order" => 3, "percent" => 80, "category" => "other", "warning_text" => null ],
            [ "name" => "Observers", "key" => "observers", "order" => 3, "percent" => 90, "category" => "other", "warning_text" => null ],
            [ "name" => "Injeção de Dependência", "key" => "dependencyInjection", "order" => 3, "percent" => 80, "category" => "other", "warning_text" => null ],
            [ "name" => "AWS - SQS, SES, S3", "key" => "aws", "order" => 2, "percent" => 90, "category" => "other", "warning_text" => null ],
            [ "name" => "HTTP / HTTPS", "key" => "http", "order" => 2, "percent" => 90, "category" => "other", "warning_text" => null ],
            [ "name" => "Arquitetura MVC", "key" => "mcv", "order" => 3, "percent" => 100, "category" => "other", "warning_text" => null ],
            [ "name" => "Arquitetura MVVM", "key" => "mvvm", "order" => 3, "percent" => 100, "category" => "other", "warning_text" => null ],
            [ "name" => "Scrum, Cambam", "key" => "sprint", "order" => 3, "percent" => 100, "category" => "other", "warning_text" => null ],
            [ "name" => "Jenkins", "key" => "jenkins", "order" => 3, "percent" => 30, "category" => "other", "warning_text" => null ],
            [ "name" => "BugSnag", "key" => "bugSnag", "order" => 3, "percent" => 100, "category" => "other", "warning_text" => null ],
            [ "name" => "FireBase", "key" => "fireBase", "order" => 3, "percent" => 100, "category" => "other", "warning_text" => null ],

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("skills");
    }
}
