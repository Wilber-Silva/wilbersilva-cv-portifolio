<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Works
 */
class Works extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company', 100);
            $table->date('begin');
            $table->date('end')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('working')->default(0);
            $table->timestamp("created_at")->useCurrent();
            $table->timestamp("updated_at")->nullable();
            $table->softDeletes();
        });

        \Illuminate\Support\Facades\DB::table("works")->insert([
            [ "company" => "4move Ltda", "begin" => "2017-03-01", "end" => null, "working" => 1 ],
            [ "company" => "ImperioSoft Studios", "begin" => "2018-09-01", "end" => null, "working" => 1 ],
            [ "company" => "Free lancer", "begin" => "2014-07-01", "end" => null, "working" => 1 ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("works");
    }
}
