<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Events
 */
class Events extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('title', 100);
            $table->enum('type_name', ["Hackathon", "Meetup"]);
            $table->date('begin');
            $table->date('end')->nullable();
            $table->text('description')->nullable();
            $table->timestamp("created_at")->useCurrent();
            $table->timestamp("updated_at")->nullable();
            $table->softDeletes();
        });

        \Illuminate\Support\Facades\DB::table("events")->insert([
            [ "name" => "DASA", "title" => "Esperiencia digital do paciente", "type_name" => "Hackathon", "begin" => "2018-12-15", "end" => "2018-12-16" ],
            [ "name" => "iFood", "title" => "Micro Serviços", "type_name" => "Meetup", "begin" => "2018-03-21", "end" => null ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("events");
    }
}
