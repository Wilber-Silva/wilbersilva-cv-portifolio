# README #

Este projeto foi criado e publicado para fim de demostração de conhecimento para pessoas técnicas que queiram conhecer o trabalho do autor. 
O projeto é publico pode ser clonado por qualquer um, apenas as apis que editam a base de dados são protegidas pela chave da aplicação contida na .env não versionada, caso queira remover a autenticação vá em Bases/BaseRequest

- laravel 5.7
- node 8.11.3
- npm 5.6.0

## GetStarted ##
### O projeto foi criado usando o laravel framework como back-end, e o react+redux para front-end até o momento não foi atribuida nenhuma responsábilidade ao redux apenas foi instalado e configurado. ###
### Caso tenha problemas ao executar o build do front-end verifique a verção do npm instalada ###

* Crie uma base de dados mySql
* Remova o .example do arquivo .env.example
* Altere as configurações da base de dados
* Execute o comando "composer install" para baixar as dependencias na pasta vender
* Execute o comando "php artisan key:generate"
* Execute o comando "php artisan migrate" para alimentar a base de dados criada
* Caso a pasta public não tenha conteudo execute o comando "npm intall && npm run dev" para efetuar o build do front-end
