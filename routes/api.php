<?php

use Illuminate\Support\Facades\Route;

Route::prefix("/skills")->group( function (){
    Route::get("/", "SkillsController@list");
} );

Route::prefix("/works")->group( function (){
    Route::get("/", "WorksController@list");
    Route::put("/", "WorksController@update");
} );

Route::prefix("/events")->group( function () {
    Route::get("/", "EventsController@list");
    Route::put("/", "EventsController@update");
} );