import React from 'react';
import PropTypes from 'prop-types';
import { Avatar, Grid, withStyles } from '@material-ui/core';

const styles = {
    avatar: {
        margin: 10,
    },
};

const ImageAvatars = ( { classes, srcPicture, width, height } ) =>(
    <Grid container justify="center" alignItems="center">
        <Avatar alt="Profile Image"
                src={ srcPicture }
                style={ { width, height } }
                className={ classes.avatar } />
    </Grid>
)

ImageAvatars.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ImageAvatars);