import React from "react";
import _ from "lodash"

import { convertMonthsAmountDate, formatDate, subDataInMonths, createMarkup } from "../../helpers";

export default ( { content } ) => (
    <ul className="timeLine">
        { content.map( ( item, key ) => (
            <li key={ key }
                data-date={ _.isUndefined( item.working ) ?  item.typeName : convertMonthsAmountDate( subDataInMonths( item.beginDate, item.endDate ) ) }>

                <div className="content">
                    <p className="title">
                        { item.title }
                        <span>
                            { formatDate( item.beginDate ) } - { item.working ? "atualmente" : formatDate( item.endDate ) }
                        </span>
                    </p>
                    <div className="body" dangerouslySetInnerHTML={ createMarkup( item.description ) } />
                </div>
            </li>
        ) ) }
    </ul>
);