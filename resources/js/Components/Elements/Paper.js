import React, { Component } from "react";
import PropTypes from 'prop-types';

import { Paper  as MaterialUiPaper, withStyles } from "@material-ui/core"

const styles = theme => ({
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        minHeight: window.innerHeight
    },
});

class Paper extends Component{
    render(){
        const { id, classes, elevation, style , children } = this.props;
        return (
            <MaterialUiPaper    id={ id } key={ id } className={ classes.root }
                                elevation={ elevation }
                                style={ style }
                                square >
                <div className="paper-body text-center mt-40 p-20">

                    { children }

                    {/*{ downTo &&*/}
                        {/*<div className="page-down" >*/}
                            {/*<a href={ `#${downTo}` }>*/}
                                {/*<i className="zmdi zmdi-chevron-down"> </i>*/}
                            {/*</a>*/}
                        {/*</div>*/}
                    {/*}*/}
                </div>
            </MaterialUiPaper>
        );
    }
}

Paper.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles( styles )( Paper );