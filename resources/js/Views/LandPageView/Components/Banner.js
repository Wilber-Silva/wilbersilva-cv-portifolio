import React from "react";

import { Typography } from "@material-ui/core";

import Paper from "../../../Components/Elements/Paper";

import { social } from "../../../Constants";

export default ( { id } ) => (
    <Paper id={ id }
           style={ {
               backgroundImage: `url(${require("../../../../assets/wallpapers/c1287d5e869f02ad2195001a3c27648e.jpg")})` ,
               backgroundRepeat: "no-repeat",
               backgroundPosition: "center",
               backgroundSize: "auto"
           } }
           elevation={ 1 }>

        <Typography variant="h5" component="h3">
            Wilber da Silva Chaves Costa.
        </Typography>

        <p>
            <span> Analista Desenvolvedor </span> / <span> Engenheiro de Software </span>, <br/>
            Entusiasta de <span> Tecnologia </span> e amante de <span> Hackathons </span>
        </p>

        { social.length &&
            <div className="social">
                { social.map( ( item, key ) => (
                    item.endpoint ?
                        <a key={ key } href={ item.endpoint } target={ "_blank" }>
                            <i className={ item.iconName }> </i>
                        </a>
                        :
                        <a key={ key } href={ "#" }>
                            <i className={ item.iconName }> </i>
                        </a>
                ) ) }
            </div>
        }
    </Paper>
);
