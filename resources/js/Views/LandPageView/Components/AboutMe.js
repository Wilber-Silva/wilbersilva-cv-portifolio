import React from "react";
import { Container, Row, Col } from "reactstrap";

import Button from "@material-ui/core/Button";

import Paper from "../../../Components/Elements/Paper";
import Avatar from "../../../Components/Avatar/CircleWithImage";

const AboutMe = ( { id } ) => (
    <Paper  id={ id }
            key={ id }
            style={ {
                backgroundColor: "#2B2B2B" ,
            } }
            elevation={ 1 } >

        <Container>
            <Row>
                <Col xs={ 12 } sm={ 12 } md={ 4 } lg={ 4 } xl={ 2 } >
                    <Avatar srcPicture={ require( "../../../../assets/avatar/0.jpg" ) } width={ 150 } height={ 150 }/>
                </Col>
                <Col xs={ 12 } sm={ 12 } md={ 8 } lg={ 8 } xl={ 10 }>
                    <Container>
                        <Row className="mb-20">
                            <Col>
                                <h2>
                                    Sobre min
                                </h2>

                                <p>
                                    Sou desenvolvedor, possuo conhecimento e experiência em desenvolvimento para plataformas Web e Android onde a Web seria a dominante no meu histórico. <br/>
                                    Costumo a fazer cursos constantes e participar de palestras para me reciclar, competições ( hackathons ) . <br/>
                                    Realizo trabalhos como Analista Desenvolvedor Full Stack, porém sou voltado mais para o back-end ou seja apensar de conhecer e usar bem linguagens e frameworks de front-end como HTML5, CSS3, JavaScript, Jquery, AngularJs, React, Redux, não é minha especialidade. <br/>
                                    Sempre uso o Git para versionar o projeto, Gihub ou Bitcket para guardar meus projetos em desenvolvimento e GitFlow quando trabalho em equipe. <br/>
                                    Hoje nas aplicações que desenvolvo uso fortemente (Saindo do padrão apenas devido ao fator cliente) o PHP ou NodeJs como linguagem back-end e LaravelPHP, MVC, e no front-end uso HTML5, CSS3 + Bootstrap, JavaScript + Jquery e ou React e no banco de dados uso mais o MySql por estar mais habituado que o MongoDB ou MariaDB. <br/>
                                    Falando do Mobile uso geralmente React Native para aplicações hibridas e no Android possuo conhecimento na arquitetura MVVM em Java, estou estudando Kotlin. <br/>
                                    Dentre as tectonologias acima sempre dou preferencia por usar as versões mais atualizadas e consolidadas.
                                </p>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={ 12 } sm={ 12 } md={ 6 } lg={ 6 } xl={ 6 }>
                                <h2>
                                    Detalhes para Contato
                                </h2>

                                <p>
                                    Wilber da Silva Chaves Costa <br/>
                                    <i className="zmdi zmdi-map"> </i> : São Paulo - Guarulhos <br/>
                                    <i className="zmdi zmdi-email"> </i> : <a href="mailto:wilberchaves@gmail.com" target="_blank"> wilberchaves@gmail.com </a>
                                    <br/>
                                    <i className="zmdi zmdi-smartphone-iphone"> </i> : <small> (11) 98789-5615 </small>
                                    <br/>
                                    <i className="zmdi zmdi-phone"> </i> : <small> (11) 2331-1705 </small>
                                </p>
                            </Col>
                            <Col xs={ 12 } sm={ 12 } md={ 6 } lg={ 6 } xl={ 6 }>
                                <a href="/pdf/cv">
                                    <Button variant="contained" size="large" color="primary">
                                        <i className="zmdi zmdi-download">  </i> <small>Baixar curriculum</small>
                                    </Button>
                                </a>
                            </Col>
                        </Row>
                    </Container>
                </Col>
            </Row>
        </Container>
    </Paper>
);

export default AboutMe;