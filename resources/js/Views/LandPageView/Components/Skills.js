import React, { Component } from "react";
import _ from "lodash";
import { Container, Row, Col, Progress } from "reactstrap";


import { Tooltip, CircularProgress } from "@material-ui/core";

import Paper from "../../../Components/Elements/Paper";
import Avatar from "../../../Components/Avatar/CircleWithImage";

const categorys = {
    back: "Back End",
    front: "Front End",
    dataBase: "Data Base",
    other: "Outros"
}

class Skills extends Component{
    state = {
        loading : false,
        skills : {
            back: [],
            dataBase: [],
            front: [],
            other: []
        }
    }
    componentWillMount(){
        this.setState( { loading: true } );

        fetch("/api/skills", { method: "GET" } )
            .then( response => response.json() )
            .then( data => this.setState( {
                loading: false,
                skills: data.data
            } ) )
            .catch( error => {
                this.setState( { loading: false } );
                console.log( error );
            } );
    }

    getLogo(key ){
        try{
            return require( `./../../../../assets/logo/${key}.png` );
        }catch(e){
            return require( "./../../../../assets/logo/empty.png" );
        }
    }
    renderCategorySkills( category ){
        return (
            <Row key={ category }>
                <p className="title">
                    { categorys[category] }
                </p>
                { this.state.skills[category].map( skill => (
                    <Col key={ skill.id } xs={ 12 } sm={ 12 } md={ 6 } lg={ 4 } xl={ 4 } className="mb-10 mt-10" >
                        <p style={ { textAlign: "left", fontSize: 18 } } >
                            { skill.name }

                            { !_.isEmpty( skill.warningText ) &&
                                <Tooltip title={ skill.warningText }>
                                    <i className="zmdi zmdi-alert-circle ml-20 text-warning"> </i>
                                </Tooltip>
                            }
                        </p>
                        <Progress value={ skill.percent }
                                  style={ { height: 35 } }>

                            {/*<img src={ this.getLogo( skill.key ) }*/}
                                 {/*alt={ skill.key }*/}
                                 {/*style={ { width: 20, height: 20, marginLeft: "48%" } } />*/}
                        </Progress>

                    </Col>
                ) ) }
            </Row>
        );
    }
    renderSkills(){

        if(
            _.isUndefined( this.state.skills.back )
            || _.isUndefined( this.state.skills.dataBase )
            || _.isUndefined( this.state.skills.front )
            || _.isUndefined( this.state.skills.other )

        ) return [];

        return [
            this.renderCategorySkills("back"),
            this.renderCategorySkills("dataBase"),
            this.renderCategorySkills("front"),
            this.renderCategorySkills("other")
        ];
    }
    render(){
        const { id } = this.props;
        return (
            <Paper  id={ id }
                    key={ id }>

                <Container>
                    { this.renderSkills() }
                </Container>

                { this.state.loading && <CircularProgress/> }
            </Paper>
        );
    }
}

export default Skills;