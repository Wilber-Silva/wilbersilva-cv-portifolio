import React, { Component } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

import Paper from "./../../../Components/Elements/Paper";
import VerticalTimeLine from "./../../../Components/TimeLine/VertialTimeLine";

class Works extends Component{
    state = {
        loading: false,
        works: []
    }
    componentDidMount(){

        this.setState( { loading: true } );

        fetch("/api/works", { method: "GET" })
            .then( response => response.json() )
            .then( data => this.setState( {
                loading: false,
                works: data.data
            } ) )
            .catch( error => {
                console.log( error );
                this.setState( { loading: false } );
            } )
    }
    render(){
        const { id } = this.props;
        return (
            <Paper  id={ id }
                    key={ id }>

                <p className="title"> Experiência </p>

                <br/>

                <VerticalTimeLine content={ this.state.works } />

                { this.state.loading && <CircularProgress/> }
            </Paper>
        );
    }
}

export default Works;