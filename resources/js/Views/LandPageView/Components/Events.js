import React, { Component } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

import Paper from "./../../../Components/Elements/Paper";
import VerticalTimeLine from "./../../../Components/TimeLine/VertialTimeLine";

class Events extends Component{
    state = {
        loading: false,
        events: []
    }
    componentDidMount(){

        this.setState( { loading: true } );

        fetch("/api/events", { method: "GET" })
            .then( response => response.json() )
            .then( data => this.setState( {
                loading: false,
                events: data.data
            } ) )
            .catch( error => {
                console.log( error );
                this.setState( { loading: false } );
            } )
    }
    render(){
        const { id } = this.props;
        return (
            <Paper  id={ id }
                    key={ id }>

                <p className="title"> Participação de Eventos </p>

                <br/>

                <VerticalTimeLine content={ this.state.events } />

                { this.state.loading && <CircularProgress/> }
            </Paper>
        );
    }
}

export default Events;