import React, { Component } from "react";
import { Link } from 'react-scroll';
import PropTypes from 'prop-types';

import { AppBar, Toolbar, IconButton, Menu, MenuItem, withStyles } from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';

const styles = theme => ({
    root: {
        width: '100%',
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
});

class Header extends Component {

    state = {
        mobileMoreAnchorEl: null,
    };

    handleMobileMenuOpen = event => {
        this.setState( { mobileMoreAnchorEl: event.currentTarget } );
    };

    handleMobileMenuClose = () => {
        this.setState( { mobileMoreAnchorEl: null } );
    };

    render() {
        const { mobileMoreAnchorEl } = this.state;
        const { classes, links } = this.props;
        const isMobileMenuOpen = Boolean( mobileMoreAnchorEl );

        const renderMobileMenu = (
            <Menu
                anchorEl={ mobileMoreAnchorEl }
                anchorOrigin={ { vertical: 'top', horizontal: 'right' } }
                transformOrigin={ { vertical: 'top', horizontal: 'right' } }
                open={ isMobileMenuOpen }
                onClose={ this.handleMobileMenuOpen } >

                { links.map( item => (
                    <MenuItem key={ `mobile-${item.key}` }>
                        <Link to={ item.key } onClick={ () => this.handleMobileMenuClose() }>{ item.name }</Link>
                    </MenuItem>
                ) ) }
            </Menu>
        );

        return (
            <div className={ classes.root } >
                <AppBar position="fixed" style={ { backgroundColor: "rgba( 0, 0, 25, 0.5)" } }>
                    <Toolbar>
                        <div className={ classes.grow } />
                        <div className={ classes.sectionDesktop }>

                            { links.map( item => (
                                <Link key={ `desktop-${item.key}` } to={ item.key } onClick={ () => this.handleMobileMenuClose() }>
                                    <IconButton color="inherit">
                                        { item.name }
                                    </IconButton>
                                </Link>
                            ) ) }
                        </div>
                        <div className={ classes.sectionMobile }>
                            <IconButton className={ classes.menuButton } color="inherit" aria-label="Open drawer" aria-haspopup="true" onClick={ this.handleMobileMenuOpen }>
                                <MenuIcon />
                            </IconButton>
                        </div>
                    </Toolbar>
                </AppBar>
                { renderMobileMenu }
            </div>
        );
    }
}

Header.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles( styles )( Header );