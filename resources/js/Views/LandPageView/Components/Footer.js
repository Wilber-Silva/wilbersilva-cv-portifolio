import React from "react";

import Paper from "./../../../Components/Elements/Paper";
import { social } from "../../../Constants";

const style = {
    width: 30,
    height: 30,
    margin: 10
}

const Footer = ( { id } ) => (
    <Paper  id={ id }
            key={ id }
            style={ {
                backgroundColor: "#000"
            } }>

        <p style={ { color: "#FFF", textAlign:"center" } }>

            Este projeto foi criado para que seja posteriormente um portfólio e um CV, o projeto é publico e pode ser clonado por qualquer pessoa, apenas a base de dados é privada e protegida pela chave da aplicação.
            <br/>
            Algumas das features criadas aqui foram criadas da forma apresenta apenas para fins de demostração para algumas não seria necessário a criação da forma em que está sendo apresentada
            <br/> <br/>
            Laravel, React, Redux
            <br/>
            <img src={ require( "../../../../assets/logo/laravelPhp.png" ) }  alt="laravelPhp" style={ style } />
            <img src={ require( "../../../../assets/logo/react.png" ) } alt="react" style={ style } />
            <img src={ require( "../../../../assets/logo/redux.png" ) } alt="redux" style={ style } />

            <br/><br/>

            { social.length &&
            <div className="social">
                { social.map( ( item, key ) => (
                    item.endpoint ?
                        <a key={ key } href={ item.endpoint } target={ "_blank" }>
                            <i className={ item.iconName }> </i>
                        </a>
                        :
                        <a key={ key } href={ "#" }>
                            <i className={ item.iconName }> </i>
                        </a>
                ) ) }
            </div>
            }

        </p>

    </Paper>
);

export default Footer;