import React, { Component } from "react";
import { connect } from "react-redux";
import { Element } from 'react-scroll'

import Header from "./Components/Header";
import Banner from "./Components/Banner";
import AboutMe from "./Components/AboutMe";
import Skills from "./Components/Skills";
import Works from "./Components/Works";
import Events from "./Components/Events";
import Footer from "./Components/Footer";

class LandPage extends Component{
    links = [
        { key: "home", name: "Home" },
        { key: "aboutMe", name: "Resumo" },
        { key: "activities", name: "Experiência Proficional" },
        { key: "events", name: "Eventos" },
        { key: "skills", name: "Habilidades" },
    ]
    render(){
        return (
            <div id={ "land-page-view" } key={ "land-page-view" } >

                <Header links={ this.links } />

                <Element name={ this.links[0].key }>
                    <Banner id={ this.links[0].key } />
                </Element>

                <Element name={ this.links[1].key }>
                    <AboutMe id={ this.links[1].key } />
                </Element>

                <Element name={ this.links[2].key }>
                    <Works id={ this.links[2].key } />
                </Element>

                <Element name={ this.links[3].key }>
                    <Events id={ this.links[3].key } />
                </Element>

                <Element name={ this.links[4].key }>
                    <Skills id={ this.links[4].key } />
                </Element>

                <Element name={ "footer" }>
                    <Footer id={ "footer" } />
                </Element>

            </div>
        );
    }
}

const mapStateToProps = ( { NavigationReducer } ) => ( { activity:  NavigationReducer.activity } );

export default connect( mapStateToProps, {
} )( LandPage );