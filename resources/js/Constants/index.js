export const social = [
    { iconName: "zmdi zmdi-facebook", endpoint: "https://www.facebook.com/will.silva.142" },
    { iconName: "zmdi zmdi-linkedin", endpoint: "https://www.linkedin.com/in/wilber-silva-010a3336/" },
    { iconName: "zmdi zmdi-instagram", endpoint: "https://www.instagram.com/wilber.silva/?hl=pt-br" },
    { iconName: "zmdi zmdi-github-alt", endpoint: "https://bitbucket.org/Wilber-Silva/wilbersilva-cv-portifolio/src/master/" },
];