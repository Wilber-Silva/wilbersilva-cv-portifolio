import { NAVIGATOR_JUMP } from "../config/redux/actionTypes";


const INITIAL_STATE = {
    activity: "home"
}

export default ( state = INITIAL_STATE, action ) => {
    switch ( action.type ){
        case NAVIGATOR_JUMP:
            return { ...state, activity: action.payload.to };
        default :
            return { ...state };
    }
}