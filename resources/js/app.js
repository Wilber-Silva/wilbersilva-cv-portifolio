import React from "react";
import ReactDOM from "react-dom"
import { Provider } from "react-redux";

import configureStore from "./config/redux/store"

import LandPage from "./Views/LandPageView";

const App = () => (
    <Provider store={ configureStore() }>
        <LandPage />
    </Provider>
);

ReactDOM.render( <App/>, document.getElementById( "root" ) );