import moment from "moment";
import _ from "lodash";


/**
 *
 * @param months
 * @returns {*}
 */
export function convertMonthsAmountDate(months){
    if(months < 12) return  ` ${months} / ${months > 1 ? "meses" : "mês"}`;
    const years = months / 12;
    const rest = months % 12;
    if( !rest ) return  ` ${parseInt( years )} / ${years > 1 ? "anos" : "ano" }` ;
    if( window.innerWidth < 400 ) return ` ${parseInt( years )} ${years > 1 ? "anos" : "ano" } / ${parseInt(rest)}` ;

    return ` ${parseInt( years )} ${years > 1 ? "anos" : "ano" } e ${parseInt(rest)} ${rest > 1 ? "meses" : "mês"}` ;
}

/**
 *
 * @param initial
 * @param finish
 * @returns {number}
 */
export function subDataInMonths( initial, finish ) {
    initial = moment( initial );
    finish = finish ? moment( finish ) : moment();
    return finish.diff( initial, "months" );
}

/**
 *
 * @param date
 * @param format
 * @returns {string}
 */
export function formatDate( date, format ) {
        if( _.isEmpty( date ) ) return "";
        if( _.isEmpty( format ) ) return moment( date ).format( "DD/MM/YYYY" );

        return moment( date ).format( format )
}

export function createMarkup( html ) {
    return { __html: html };
}