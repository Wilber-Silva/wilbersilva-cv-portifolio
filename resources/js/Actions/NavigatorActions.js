import { NAVIGATOR_JUMP } from "./../config/redux/actionTypes";

export const jump = ( payload ) => ({
    type: NAVIGATOR_JUMP,
    payload
});