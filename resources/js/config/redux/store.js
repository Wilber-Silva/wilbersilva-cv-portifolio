import { createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";

import Reducers from "../../Reducers"

export default ( initialState ) => createStore(
    Reducers,
    initialState,
    applyMiddleware( ReduxThunk )
) ;