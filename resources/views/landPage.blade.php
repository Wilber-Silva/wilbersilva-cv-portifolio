<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no"/>
        <meta name="theme-color" content="#000000">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta property="og:url" content="{{ env("APP_URL") }}">
        <meta property="og:site_name" content="{{ env("APP_NAME") }}">
        {{--<meta property="article:author" content="">--}}
        {{--<meta property="article:publisher" content="">--}}
        {{--<meta property="twitter:domain" content="">--}}
        {{--<meta property="twitter:title" content="">--}}
        {{--<meta property="og:type" content="article">--}}
        {{--<meta property="twitter:description" content="">--}}
        {{--<meta property="twitter:image" content="">--}}
        {{--<meta property="twitter:url" content="">--}}
        {{--<meta name="robots" content="index, follow">--}}

        {{--<link rel="sitemap" type="application/xml" href="">--}}
        {{--<link rel="alternate" type="application/rss+xml" href="">--}}


        <!--
          manifest.json provides metadata used when your web app is added to the
          homescreen on Android. See https://developers.google.com/web/fundamentals/engage-and-retain/web-app-manifest/
        -->
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
        <!-- Use For Material Icons -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- Use For Material Iconic Font -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
        <!-- Use For Simple Line Icon -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" />
        <!-- Use For Leaflet Map -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.1.0/leaflet.css" />
        <!-- Use For Jvector Map -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jvectormap/2.0.4/jquery-jvectormap.css" type="text/css"
              media="screen" />
        <!-- Use For Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Heebo:100,300,400,500,700,800,900" rel="stylesheet">
        <!-- Typography -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">

        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
        <!--
          Notice the use of %PUBLIC_URL% in the tags above.
          It will be replaced with the URL of the `public` folder during the build.
          Only files inside the `public` folder can be referenced from the HTML.

          Unlike "/favicon.ico" or "favicon.ico", "%PUBLIC_URL%/favicon.ico" will
          work correctly both with client-side routing and a non-root public URL.
          Learn how to configure a non-root public URL by running `npm run build`.
        -->
        <title> {{ env("APP_NAME") }} </title>
    </head>

    <body>
        <noscript>
            You need to enable JavaScript to run this app.
        </noscript>
        <div id="root"></div>
        <!--
          This HTML file is a template.
          If you open it directly in the browser, you will see an empty page.

          You can add webfonts, meta tags, or analytics to this file.
          The build step will place the bundled scripts into the <body> tag.

        -->
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    </body>

</html>