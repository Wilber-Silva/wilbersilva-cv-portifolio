<?php

namespace App\Providers;

use App\Bases\Model\BaseModel;
use App\Models\Works;
use App\Observers\BaseModelObserver;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        BaseModel::observe(BaseModelObserver::class);
        Works::observe(BaseModelObserver::class);
    }
}
