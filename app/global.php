<?php


/***********************************************************************************************************************
 * Http status code
 */
const HTTP_STATUS_SUCCESS = 200;
const HTTP_STATUS_BAD_REQUEST = 400;
const HTTP_STATUS_UNAUTHORIZED = 401;
const HTTP_STATUS_EXPIRE_ACCESS = 419;
const HTTP_STATUS_NOT_FOUND = 404;
const HTTP_UNPROCESSABLE_ENTITY = 422;
const HTTP_INTERNAL_SERVER_ERROR = 500;
const HTTP_CUSTOM_INTERNAL_SERVER_ERROR = 506;


const AUTH_GUARD_KEY = "currentUserGuard";