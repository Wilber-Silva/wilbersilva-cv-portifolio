<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 21/03/2019
 * Time: 18:31
 */

namespace App\Services;

use App\Bases\Service\BaseService;
use App\Repositories\SkillsRepository;

/**
 * Class SkillsService
 * @package App\Servicies
 */
class SkillsService extends BaseService{

    /**
     * @return \Illuminate\Support\Collection|static
     */
    static function listGrouped(){
        $list = SkillsRepository::getAllSkills();

        if( !$list->count() ) return $list;

        return $list->groupBy("category");
    }
}