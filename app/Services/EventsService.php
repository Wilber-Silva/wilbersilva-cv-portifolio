<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 22/03/2019
 * Time: 19:42
 */

namespace App\Services;

use App\Bases\Service\BaseService;
use App\Repositories\EventsRepository;

/**
 * Class EventsService
 * @package App\Services
 */
class EventsService extends BaseService{

    /**
     * EventsService constructor.
     * @param EventsRepository $repository
     */
    public function __construct( EventsRepository $repository ){
        parent::__construct( $repository );
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    static function listRecentEvents(){
        return EventsRepository::getRecentEvents();
    }

    /**
     * @param int|null $id
     * @return $this|null
     */
    static function find( ?int  $id ){
        $repository = EventsRepository::find( $id );

        if ( !$repository ) return null;

        return new self( $repository );
    }

    /**
     * @param array $inputs
     * @return object
     */
    static function update( array $inputs ){
        $eventsService = self::find( $inputs["id"] );

        if ( !$eventsService ) return self::handle( HTTP_INTERNAL_SERVER_ERROR );

        $eventsService->adapter( $inputs );
        return $eventsService->save();
    }
}