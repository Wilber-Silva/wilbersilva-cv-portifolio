<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 22/03/2019
 * Time: 18:17
 */

namespace App\Services;

use App\Bases\Service\BaseService;
use App\Repositories\WorksRepository;

/**
 * Class Works
 * @package App\Servicies
 */
class WorksService extends BaseService{

    /**
     * WorksService constructor.
     * @param WorksRepository $repository
     */
    function __construct( WorksRepository $repository ){
        parent::__construct( $repository );
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    static function listImportantRecentWorks(){
        return WorksRepository::getImportantRecentWorks();
    }

    /**
     * @param int|null $id
     * @return WorksService|null
     */
    static function find( ?int $id ){
        $workRepository = WorksRepository::find( $id );

        if( !$workRepository ) return null;

        return new self( $workRepository );
    }

    /**
     * @param array $inputs
     * @return object
     */
    static function update( array $inputs ){
        $workService = self::find( $inputs["id"] );

        if( !$workService ) return self::handle( HTTP_STATUS_NOT_FOUND );

        $workService->adapter( $inputs );
        return $workService->save();
    }
}