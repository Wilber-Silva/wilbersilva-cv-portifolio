<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 23/03/2019
 * Time: 14:34
 */

namespace App\Http\Requests\Works;

use App\Bases\BaseRequest;

/**
 * Class WorkUpdateRequest
 * @package App\Http\Requests\Works
 */
class WorkUpdateRequest extends BaseRequest{

    /**
     * @description validation rules
     *
     * @return array
     */
    public function rules(){
        // TODO: Implement rules() method.
        return [
            "id" => "required|numeric|exists:works",
            "company" => "max:100",
            "working" => "bool"
        ];
    }
}