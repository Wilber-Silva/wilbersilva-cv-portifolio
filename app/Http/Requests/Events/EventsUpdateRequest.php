<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 24/03/2019
 * Time: 09:31
 */

namespace App\Http\Requests\Events;

use App\Bases\BaseRequest;

/**
 * Class EventsUpdateRequest
 * @package App\Http\Requests\Events
 */
class EventsUpdateRequest extends BaseRequest{

    /**
     * @description validation rules
     *
     * @return array
     */
    public function rules(){
        // TODO: Implement rules() method.
        return [
            "id" => "required|numeric|exists:events",
            "name" => "max:100",
            "title" => "max:100"
        ];
    }
}