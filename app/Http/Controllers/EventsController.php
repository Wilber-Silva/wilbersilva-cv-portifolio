<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 22/03/2019
 * Time: 19:40
 */

namespace App\Http\Controllers;

use App\Bases\BaseController;
use App\Http\Requests\Events\EventsUpdateRequest;
use App\Services\EventsService;

/**
 * Class EventsController
 * @package App\Http\Controllers
 */
class EventsController extends BaseController{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    function list(){
        $this->setResponseStatusCode( HTTP_STATUS_SUCCESS );
        $this->setResponseBody( EventsService::listRecentEvents() );
        return $this->jsonResponse();
    }

    /**
     * @param EventsUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    function update( EventsUpdateRequest $request ){
        $this->setResponseStatusCode( EventsService::update( $request->all() )->statusCode );
        return $this->jsonResponse();
    }
}