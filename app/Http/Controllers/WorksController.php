<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 22/03/2019
 * Time: 18:15
 */

namespace App\Http\Controllers;

use App\Bases\BaseController;
use App\Http\Requests\Works\WorkUpdateRequest;
use App\Services\WorksService;

/**
 * Class WorksController
 * @package App\Http\Controllers
 */
class WorksController extends BaseController{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    function list(){
        $this->setResponseStatusCode( HTTP_STATUS_SUCCESS );
        $this->setResponseBody( WorksService::listImportantRecentWorks() );
        return $this->jsonResponse();
    }

    /**
     * @param WorkUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    function update( WorkUpdateRequest $request ){
        $this->setResponseStatusCode( WorksService::update( $request->all() )->statusCode );
        return $this->jsonResponse();
    }
}