<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 21/03/2019
 * Time: 18:27
 */

namespace App\Http\Controllers;

use App\Bases\BaseController;
use App\Services\SkillsService;

/**
 * Class SkillsController
 * @package App\Http\Controllers
 */
class SkillsController extends BaseController{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    function list(){
        $this->setResponseStatusCode( HTTP_STATUS_SUCCESS );
        $this->setResponseBody( SkillsService::listGrouped() );
        return $this->jsonResponse();

    }
}