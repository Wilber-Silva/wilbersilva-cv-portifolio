<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 26/02/2019
 * Time: 10:41
 */

namespace App\Traits;

/**
 * Trait ResponseJson
 * @package App\Traits
 */
trait ResponseJson{

    /**
     * @var array *** defaults headers json response
     */
    private $headerJsonResponse = [
        "Content-Type:application/x-www-form-urlencoded"
    ];

    /**
     * @var int
     */
    private $responseStatusCode = HTTP_STATUS_SUCCESS;
    /**
     * @var string
     */
    protected $responseMessage = null;

    /**
     * @var array
     */
    protected $responseBody = [];

    /**
     * @param int $statusCode
     * @return void
     */
    protected function setResponseStatusCode(int $statusCode){
        $this->responseStatusCode = $statusCode;
    }

    /**
     * @param string|null $message
     */
    protected function setResponseMessage(?string $message){
        $this->responseMessage = $message;
    }

    /**
     * @param $body
     */
    protected function setResponseBody($body){
        $this->responseBody = $body;
    }

    /**
     * @param array $messages
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonResponse(array $messages = []){
        // must use another response methods after json() method
        $data = [
            "data" => $this->responseBody
        ];
        if(count($messages)) $data["messages"] = $messages;

        if(!$this->responseMessage)
            switch ($this->responseStatusCode){
                case HTTP_STATUS_SUCCESS:
                    $data["message"] = "Operação realizada com sucesso";
                    break;
                case HTTP_UNPROCESSABLE_ENTITY:
                    $data["message"] = "Preencha os campos corretamente";
                    break;
                case HTTP_STATUS_UNAUTHORIZED:
                    $data["message"] = "Acesso não altorizado";
                    break;
                default:
                    $data["message"] = "Erro interno";
                    break;
            }
        else
            $data["message"] = $this->responseMessage;

        return response()
            ->json($data) // use json_encode()
            ->setStatusCode($this->responseStatusCode)
            ->withHeaders($this->headerJsonResponse);
    }
}