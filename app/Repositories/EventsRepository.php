<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 22/03/2019
 * Time: 19:41
 */

namespace App\Repositories;

use App\Bases\Repository\BaseRepository;
use App\Models\Events;
use Illuminate\Support\Facades\DB;

/**
 * Class EventsRepository
 * @package App\Repositories
 */
class EventsRepository extends BaseRepository{

    /**
     * EventsRepository constructor.
     * @param Events $modelObject
     */
    function __construct(Events $modelObject){
        parent::__construct($modelObject);
    }

    /**
     * @param int|null $id
     * @return EventsRepository|null
     */
    static function find( ?int $id ){
        $events = Events::find( $id );

        if ( !$events ) return null;

        return new self( $events );
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    static function getRecentEvents(){
        try{
            return Events::orderBy("begin", "DESC")
                            ->orderBy("id", "DESC")
                            ->select(
                                "id",
                                DB::raw( "concat( name, ', ', title ) as title" ),
                                "type_name as typeName",
                                "begin as beginDate",
                                "end as endDate",
                                "description"
                            )
                            ->take(5)
                            ->get();
        }catch (\Exception $e){
            return collect([]);
        }
    }

    /**
     * @param object $data
     * @return $this|null
     */
    static function insert($data){
        // TODO: Implement insert() method.
        return null;
    }

    /**
     * @param object $data
     * @return bool
     */
    function update($data){
        // TODO: Implement update() method.
        return false;
    }
}