<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 21/03/2019
 * Time: 18:32
 */

namespace App\Repositories;

use App\Bases\Repository\BaseRepository;
use App\Models\Skills;

/**
 * Class SkillsRepository
 * @package App\Repositories
 */
class SkillsRepository extends BaseRepository{

    /**
     * SkillsRepository constructor.
     * @param Skills $modelObject
     */
    function __construct(Skills $modelObject){
        parent::__construct($modelObject);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    static function getAllSkills(){
        try{
            return Skills::orderBy("order", "ASC")
                            ->orderBy("name", "ASC")
                            ->select(
                                "id",
                                "name",
                                "key",
                                "category",
                                "percent",
                                "warning_text as warningText"
                            )
                            ->get();
        }catch (\Exception $e){
            return collect([]);
        }
    }

    /**
     * @param object $data
     * @return $this|null
     */
    static function insert($data){
        // TODO: Implement insert() method.
        return null;
    }

    /**
     * @param object $data
     * @return bool
     */
    function update($data){
        // TODO: Implement update() method.
        return false;
    }
}