<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 22/03/2019
 * Time: 18:18
 */

namespace App\Repositories;

use App\Bases\Repository\BaseRepository;
use App\Models\Works;

/**
 * Class WorksRepository
 * @package App\Repositories
 */
class WorksRepository extends BaseRepository{

    /**
     * WorksRepository constructor.
     * @param Works $modelObject
     */
    function __construct( Works $modelObject ){
        parent::__construct( $modelObject );
    }

    /**
     * @param int|null $id
     * @return $this|null
     */
    static function find( ?int $id ){
        try{
            $work = Works::find( $id );

            if( !$work ) return null;

            return new self( $work );

        }catch (\Exception $e){
            return null;
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    static function getImportantRecentWorks(){
        try{
            return Works::orderBy("begin", "DESC")
                            ->orderBy("end", "DESC")
                            ->orderBy("working", "DESC")
                            ->orderBy("id", "ASC")
                            ->select(
                                "id",
                                "company as title",
                                "begin as beginDate",
                                "end as endDate",
                                "description",
                                "working"
                            )
                            ->take(3)
                            ->get();
        }catch (\Exception $e){
            return collect([]);
        }
    }

    /**
     * @param object $data
     * @return $this|null
     */
    static function insert( $data ){
        // TODO: Implement insert() method.
        return null;
    }

    /**
     * @param object $data
     * @return bool
     */
    function update( $data ){
        // TODO: Implement update() method.
        return false;
    }
}