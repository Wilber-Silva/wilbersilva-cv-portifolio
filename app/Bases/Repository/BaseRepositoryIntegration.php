<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 15/01/2019
 * Time: 10:28
 */

namespace App\Bases\Repository;
use App\Bases\BaseIntegration;

/**
 * Class BaseRepositoryIntegration
 * @package App\Bases\Repository
 */
class BaseRepositoryIntegration {

    /**
     * @var BaseIntegration
     */
    protected $integration;

    /**
     * BaseRepositoryIntegration constructor.
     * @param BaseIntegration $integration
     */
    public function __construct(BaseIntegration $integration){
        $this->integration = $integration;
    }

    /**
     * @param int $statusCode
     * @param mixed $body
     * @return object
     */
    static function handle(int $statusCode, $body = null){
        return (object) [
            "statusCode" => $statusCode,
            "body" => $body
        ];
    }
}