<?php

namespace App\Bases\Repository;

use App\Bases\Model\BaseModel;

/**
 * Class BaseRepository
 * @package App\Repositories
 *
 * @description BaseRepository have general Repositories methods
 *
 * @author Wilber da Silva Chaves Costa <wilber.silva@4move.me>
 */
abstract class BaseRepository{

    /**
     * @var string
     */
    protected static $model = BaseModel::class;

    /**
     * @var BaseModel
     */
    protected $dataObject;

    /**
     * @var array
     */
    protected $originalDate;

    /**
     * @var int
     */
    protected static $paginate = 30;

    /**
     * BaseRepository constructor.
     *
     * @description super::class constructor
     *
     * @param BaseModel $modelObject = You can be use BaseModelSon::class
     */
    protected function __construct(BaseModel $modelObject){
        $this->dataObject = $modelObject;
//        dd($this->dataObject->getObservableEvents());
    }

    /**
     * @return BaseModel
     */
    public function getDataObject(){
        return $this->dataObject;
    }

    /**
     * @param $index
     * @param $value
     *
     * @return void
     */
    public function setIntoDataObjectWithIndex( $index, $value ){
        $this->dataObject->$index = $value;
    }

    /**
     * @param object $data
     * @return $this|null
     */
    abstract static function insert($data);

    /**
     * @param object $data
     * @return bool
     */
    abstract function update($data);

    /**
     * @return bool
     */
    function save(){
        try{
            $this->dataObject->save();

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    /**
     * @description delete current dataObject
     *
     * @return bool
     */
    function delete(){
        try{
            $this->dataObject->delete();

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

}