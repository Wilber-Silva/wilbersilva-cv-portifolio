<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 12/12/2018
 * Time: 09:54
 */

namespace App\Bases;

use GuzzleHttp\Client;

/**
 * Class BaseIntegration
 * @package App\Integrations\Base
 */
class BaseIntegration extends Client{

    public $response;

    protected $requestStatusCode = HTTP_STATUS_SUCCESS;
    protected $baseUrl = "";
    protected $version = "";

    const GET_METHOD = "GET";
    const POST_METHOD = "POST";
    const PUT_METHOD = "PUT";
    const DELETE_METHOD = "DELETE";

    /**
     * BaseIntegration constructor.
     * @param array $config
     */
    public function __construct(array $config = [ 'curl' => [CURLOPT_SSL_VERIFYPEER => false, CURLOPT_RETURNTRANSFER, false ], ]){
        parent::__construct($config);
    }

    /**
     * @param array $query
     * @return string
     */
    static function httpQuery(array $query){
        return http_build_query($query);
    }

    /**
     * @param int $statusCode
     * @param mixed $body
     * @return object
     */
    static function handle(int $statusCode, $body = null){
        return (object) [
            "statusCode" => $statusCode,
            "body" => $body
        ];
    }

}