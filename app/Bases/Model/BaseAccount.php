<?php

namespace App\Bases\Model;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticatableTrait;
use Illuminate\Support\Str;

/**
 * Class BaseAccount
 * @package App\Models\Base
 */
class BaseAccount
    extends BaseModel
    implements Authenticatable {

    use AuthenticatableTrait;

    public static $columns = [
        "email" => "email",
        "firstName" => "first_name",
        "lastName" => "last_name",
        "fullName" => "full_name",
        "documentNumber" => "document_number",
        "phoneNumber" => "phone",
        "codeId" => "my_code",
        "networkCodeId" => "networking_code",
        "customerTypeId" => "customer_type",
        "rememberToken" => "remember_token",
        "accessToken" => "token",
        "picture" => "picture",
        "password" => "password",
    ];

    function updateAccessToken(){
        $this->setter("token", Str::random(60));
        $this->save();
    }

    /*******************************************************************************************************************
     * Getters @methods
     */

    /**
     * @return string
     */
    function getEmail(){ return $this::getter(self::$columns["email"]); }

    /**
     * @return string
     */
    function getFullName(){
        if($this::getter(self::$columns["lastName"]))
            return $this::getter(self::$columns["firstName"]) . " " . $this::getter(self::$columns["lastName"]);
        else
            return $this::getter(self::$columns["firstName"]);
    }

    /**
     * @return string|number
     */
    function getDocumentNumber(){ return $this::getter(self::$columns["documentNumber"]); }

    /**
     * @return string|number
     */
    function getPhoneNumber(){ return $this::getter(self::$columns["phoneNumber"]); }

    /**
     * @return int
     */
    function getCodeId(){ return $this::getter(self::$columns["codeId"]); }

    /**
     * @return int
     */
    function getNetworkCodeId(){ return $this->getter(self::$columns["networkCodeId"]); }

    /**
     * @return int
     */
    function getCustomerTypeId(){ return $this::getter(self::$columns["customerTypeId"]); }

    /**
     * @return string
     */
    function getPicture(){ return $this->getter(self::$columns["picture"]); }

    /**
     * @return string
     */
    function getHashedPassword(){ return $this->getter(self::$columns["password"]); }

    /**
     * @return mixed
     */
    function getAccessToken(){ return $this->getter(self::$columns["accessToken"]); }
}