<?php

namespace App\Bases;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Traits\ResponseJson;

/**
 * Class BaseController
 * @package App\Http\Controllers\Base
 */
class BaseController extends Controller{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ResponseJson;

}
