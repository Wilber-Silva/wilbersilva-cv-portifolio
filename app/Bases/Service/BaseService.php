<?php

namespace App\Bases\Service;

use App\Bases\Repository\BaseRepository;
use Illuminate\Http\UploadedFile;


/**
 * Class BaseService
 * @package App\Services
 *
 * @author Wilber da Silva Chaves Costa <wilber.silva@4move.me>
 */
class BaseService{

    /**
     * @var BaseRepository
     */
    protected $repository;

    /**
     * BaseService constructor.
     * @param BaseRepository $repository
     */
    public function __construct(BaseRepository $repository){
        $this->repository = $repository;
    }


    /**
     * @param UploadedFile $uploadedFile
     * @param array $bucket
     * @param null|string $customerName
     * @return object
     */
    function uploadFile(UploadedFile $uploadedFile, Array $bucket, ?string $customerName = "file"){
//        $fileName = $customerName . '-' . time() . '.' . $uploadedFile->extension();
//        try{
//            $s3 = new S3($fileName, $bucket);
//            $s3->putObject($uploadedFile->getPathname());
//            $s3->setObjectPublic($uploadedFile->getMimeType());
//            return self::handle(HTTP_STATUS_SUCCESS, (object) [
//                "fileEndpoint" => $s3->getUrl()
//            ]);
//
//        }catch (\Exception $e){
//            return self::handle(HTTP_INTERNAL_SERVER_ERROR);
//        }
    }
    /**
     * @param string $path
     * @param array $bucket
     * @return object
     */
    static function deleteFile(string $path, array $bucket){
//        try{
//
//            $s3 = new S3($path, $bucket);
//            $s3->delete();
//
//            return self::handle(HTTP_INTERNAL_SERVER_ERROR);
//        }catch (\Exception $e){
//            return self::handle(HTTP_INTERNAL_SERVER_ERROR);
//        }
    }

    /**
     * @param array $inputs
     */
    function adapter( array $inputs ){
        unset( $inputs["id"] );
        unset( $inputs["_token"] );
        unset( $inputs["_method"] );
        foreach ( $inputs as $index => $value ){
            $this->repository->setIntoDataObjectWithIndex( $index, $value );
        }
    }

    /**
     * @return object
     */
    function save(){
        if(!$this->repository->save()) return $this::handle(HTTP_INTERNAL_SERVER_ERROR);

        return $this::handle(HTTP_STATUS_SUCCESS);
    }

    /**
     * @description Delete into database (Using softDeletes)
     *
     * @return object
     */
    function delete(){

        if(!$this->repository->delete()) return $this::handle(HTTP_INTERNAL_SERVER_ERROR);

        return $this::handle(HTTP_STATUS_SUCCESS);
    }

    /**
     * @param int $statusCode
     * @param string|null $message
     * @param mixed $data
     * @return object
     */
    static function handle(int $statusCode, $data = [], ?string $message = null){
        return handle($statusCode, $data, $message);
    }
}