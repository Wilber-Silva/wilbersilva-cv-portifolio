<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 15/01/2019
 * Time: 10:34
 */

namespace App\Bases\Service;

use App\Bases\Repository\BaseRepositoryIntegration;

/**
 * Class BaseServiceIntegration
 * @package App\Bases\Service
 */
class BaseServiceIntegration{

    /**
     * @var BaseRepositoryIntegration
     */
    protected $integrationRepository;

    /**
     * BaseServiceIntegration constructor.
     * @param BaseRepositoryIntegration $integration
     */
    public function __construct(BaseRepositoryIntegration $integration){
        $this->integrationRepository = $integration;
    }
}