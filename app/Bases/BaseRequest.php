<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 14/12/2018
 * Time: 16:54
 */

namespace App\Bases;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class BaseRequest
 * @package App\Http\Requests\Base
 */
abstract class BaseRequest extends FormRequest {

    /**
     * @description toggle authorization rules
     *
     * @return bool
     */
    function authorize(){
        return request()->header("app-key") == env("APP_KEY");
    }

    /**
     * @description validation rules
     *
     * @return array
     */
    abstract public function rules();

    /**
     * @param Validator $validator
     */
    public function failedValidation(Validator $validator){
        throw new HttpResponseException(response()->json(
            [
                "errors" => $validator->errors(),
                "message" => "Pressncha os campos corretamente"
            ],
            HTTP_UNPROCESSABLE_ENTITY
        ));
    }
}