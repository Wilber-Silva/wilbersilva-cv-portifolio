<?php

namespace App\Observers;

use App\Bases\Model\BaseModel;
use App\Models\GeneralLog;
use Illuminate\Support\Facades\Log;

/**
 * Class BaseModelObserver
 * @package App\Observers
 */
class BaseModelObserver{

    const INSERT_OPERATION = 'INSERT';
    const UPDATE_OPERATION = 'UPDATE';
    const DELETE_OPERATION = 'DELETE';
    const FORCE_DELETE_OPERATION = 'FORCE_DELETE';

    /**
     * Handle the base model "created" event.
     *
     * @param  BaseModel  $baseModel
     * @return void
     */
    function created( BaseModel $baseModel ){
        try{
            $log = new GeneralLog();
            $log->setter("origin_table", $baseModel->getTable());
            $log->setter("type_operation", self::INSERT_OPERATION);
            $log->setter("log", null);
            $log->save();
        }catch (\Exception $e){
            Log::info( $e );
        }
    }

    /**
     * Handle the base model "updated" event.
     *
     * @param  BaseModel  $baseModel
     * @return void
     */
    function updated( BaseModel $baseModel ){
        try{
            $log = new GeneralLog();
            $log->setter("origin_table", $baseModel->getTable());
            $log->setter("type_operation", self::UPDATE_OPERATION);
            $log->setter("log", json_encode((object) [
                "original" => $baseModel->getOriginal(),
                "changes" => $baseModel->getChanges()
            ]));
            $log->save();
        }catch (\Exception $e){
            Log::info( $e );
        }
    }

    /**
     * Handle the base model "saved" event.
     *
     * @param  BaseModel  $baseModel
     * @return void
     */
    function saved( BaseModel $baseModel ){
    }

    /**
     * Handle the base model "deleted" event.
     *
     * @param  BaseModel  $baseModel
     * @return void
     */
    function deleted( BaseModel $baseModel ){
        try{
            $log = new GeneralLog();
            $log->setter("origin_table", $baseModel->getTable());
            $log->setter("type_operation", self::DELETE_OPERATION);
            $log->setter("log", json_encode((object) [
                "original" => $baseModel->getOriginal()
            ]));
            $log->save();
        }catch (\Exception $e){
            Log::info( $e );
        }
    }

    /**
     * Handle the base model "restored" event.
     *
     * @param  BaseModel  $baseModel
     * @return void
     */
    function restored(BaseModel $baseModel)
    {
        //
    }

    /**
     * Handle the base model "force deleted" event.
     *
     * @param  BaseModel  $baseModel
     * @return void
     */
    function forceDeleted(BaseModel $baseModel)
    {
        //
    }
}
