<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 22/03/2019
 * Time: 19:26
 */

namespace App\Models;


use App\Bases\Model\BaseModel;

/**
 * Class Events
 * @package App\Models
 */
class Events extends BaseModel{
    protected $table = "events";
}