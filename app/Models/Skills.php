<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 21/03/2019
 * Time: 17:34
 */

namespace App\Models;

use App\Bases\Model\BaseModel;

/**
 * Class Skills
 * @package App\Models
 */
class Skills extends BaseModel{
    protected $table = "skills";
}