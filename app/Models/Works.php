<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 22/03/2019
 * Time: 18:16
 */

namespace App\Models;

use App\Bases\Model\BaseModel;

/**
 * Class Works
 * @package App\Models
 */
class Works extends BaseModel{
    protected $table = "works";
}