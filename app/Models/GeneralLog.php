<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 23/03/2019
 * Time: 15:20
 */

namespace App\Models;


use App\Bases\Model\BaseModel;

/**
 * Class GeneralLog
 * @package App\Models
 */
class GeneralLog extends BaseModel{
    protected $table = "log";
}