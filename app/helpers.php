<?php

use Illuminate\Support\Facades\Hash;

if(!function_exists("current_user")){
    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|App\Bases\Model\BaseAccount|\App\Models\User
     */
    function current_user(){
        return \Illuminate\Support\Facades\Auth::guard(session(AUTH_GUARD_KEY))->user();
    }
}

if(!function_exists('clean')) {
    /**
     * @param $string
     * @return null|string|string[]
     */
    function clean($string){
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
}

if(!function_exists('generate_token')) {
    /**
     * @return null|string|string[]
     */
    function generate_token(){
        return clean(Hash::make(rand() .time() . rand()));
    }
}

if(!function_exists('brazilian_currency_format')){

    /**
     * @param int|float|double $value
     * @return string
     */
    function brazilian_currency_format($value){
        return number_format($value, 2, ",", ".");

    }
}

if(!function_exists("handle")){

    /**
     * @param int $statusCode
     * @param string|null $message
     * @param object $data
     *
     * @return object
     */
    function handle(int $statusCode, $data = null, ?string $message){
        return (object) [
            "statusCode" => $statusCode,
            "message" => $message,
            "data" => $data
        ];
    }
}

if(!function_exists("id_field")){
    /**
     * @param $id
     * @return \Illuminate\Support\HtmlString
     */
    function id_field(int $id){
        return new \Illuminate\Support\HtmlString("<input type='hidden' name='id' value='$id'>");
    }
}
if(!function_exists("convert_to_cents")){
    /**
     * @param float|double|int $value
     * @return int
     */
    function convert_to_cents($value){
        return (int) ($value * 100);
    }
}
